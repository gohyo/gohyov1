var express = require('express');
var model = require('../model');
var passwordHash = require('password-hash');
var router = express.Router();
/* Create a new user category */


router.post('/categories', (req, res) => {
    var body = req.body;
    model.user_category.create({
        user_category_name: body.category_name
    }).then(data => {
        res.json({
            stat: 'success',
            code: 201,
            id: data.id
        });
    }, error => {
        res.json({
            stat: 'failure',
            code: 300,
            message: error
        })
    })
})

/* Get all categories */
router.get('/categories', (req, res) => {
    model.user_category.findAll().then(data => {
        res.json({
            category_count: data.length,
            categories: data
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301,
            message: error
        })
    })
})

/* Get category by id */
router.get('/categories/:id', (req, res) => {
    model.user_category.findOne({
        where: {
            id: req.params.id
        }
    }).then(data => {
        res.json({
            category: data,
            stat: data ? 200 : 202
        })
    }, error => {
        res.json({
            code: 301,
            stat: 'error',
            message: error
        })
    })
})

/*Create a new generic user*/

router.post('/generic', (req, res) => {
    var body = req.body;
    model.user.create({
        name: body.name,
        email_id: body.email_id,
        password: passwordHash.generate(body.password),
        mobile_number: body.mobile_no,
        phone_no: body.phone_no,
        profile_pic_path: body.profile_pic,
        active: body.is_active,
        userCategoryId: body.user_categoryid
    }).then(data => {
        res.json({
            id: data.id,
            stat: 'success',
            code: 200
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301,
            message: error
        })
    })
})

/*Create individual user*/
router.post('/individual', (req, res) => {
    var body = req.body;
    model.user.create({
        name: body.name,
        email_id: body.email_id,
        password: passwordHash.generate(body.password),
        mobile_number: body.mobile_no,
        phone_no: body.phone_no,
        profile_pic_path: body.profile_pic,
        active: body.is_active,
        userCategoryId: body.user_categoryid
    }).then(data => {
        model.individual_user_auxilliary.create({
            pan: body.pan,
            pan_verified: body.pan_verified,
            email_hash: body.email_hash,
            email_verified: body.email_verified,
            userId: data.id
        }).then(data2 => {
            res.json({
                id: data2.id,
                stat: 'success',
                code: 200
            })
        }, error2 => {
            res.json({
                stat: 'failure',
                code: 300
            })
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301,
            message: error
        })
    })
})

/*Get a specific individual user*/
router.get('/individual/:id', (req, res) => {
    model.user.findOne({
        where: {
            id: req.params.id
        },
        attributes: {
            exclude: ['password']
        },
        include: [{
            model: model.user_category,
            as: 'user_category',
            attributes: ['user_category_name']
        }, model.individual_user_auxilliary]
    }).then(data => {
        res.json({
            user: data
        })
    }, error => {
        console.log(error);
        res.json({
            stat: 'failure',
            code: 301
        })
    })
})

/*Corporate User*/
router.post('/corporate', (req, res) => {
    var body = req.body;
    model.user.create({
        name: body.name,
        email_id: body.email_id,
        password: passwordHash.generate(body.password),
        mobile_number: body.mobile_no,
        phone_no: body.phone_no,
        profile_pic_path: body.profile_pic,
        active: body.is_active,
        userCategoryId: body.user_categoryid
    }).then(data => {
        model.retail_corp_user_auxilliary.create({
            pan: body.pan,
            pan_verified: body.pan_verified,
            vat_num: body.vat_num,
            st_num: body.st_num,
            tan_num: body.tan_num,
            payment_cycle: body.payment_cycle,
            userId: data.id
        }).then(data2 => {
            res.json({
                id: data2.id,
                stat: 'success',
                code: 200
            })
        }, error2 => {
            res.json({
                stat: 'failure',
                code: 300
            })
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301,
            message: error
        })
    })
})

/*Get specific corporate user*/
router.get('/corporate/:id', (req, res) => {
    model.user.findOne({
        where: {
            id: req.params.id
        },
        attributes: {
            exclude: ['password']
        },
        include: [{
            model: model.user_category,
            as: 'user_category',
            attributes: ['user_category_name']
        }, model.retail_corp_user_auxilliary]
    }).then(data => {
        res.json({
            user: data
        })
    }, error => {
        console.log(error);
        res.json({
            stat: 'failure',
            code: 301
        })
    })
})

/*Create a fleet user*/
router.post('/fleetusers', (req, res) => {
    var body = req.body;
    model.user.create({
        name: body.name,
        email_id: body.email_id,
        password: passwordHash.generate(body.password),
        mobile_number: body.mobile_no,
        phone_no: body.phone_no,
        profile_pic_path: body.profile_pic,
        active: body.is_active,
        userCategoryId: body.user_categoryid
    }).then(data => {
        model.fleet_owner_auxilliary.create({
            bank_name: body.bank_name,
            bank_branch: body.bank_branch,
            bank_accntNo: body.bank_accntNo,
            bank_ifsc: body.bank_ifsc,
            tds_status: body.tds_status,
            payment_period: body.payment_period,
            userId: data.id
        }).then(data2 => {
            res.json({
                id: data2.id,
                stat: 'success',
                code: 200
            })
        }, error2 => {
            res.json({
                stat: 'failure',
                code: 300
            })
            console.log(error2);
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301,
            message: error
        })

    })
})


/*Get a specific fleet owner*/
router.get('/fleetusers/:id', (req, res) => {
    model.user.findOne({
        where: {
            id: req.params.id
        },
        attributes: {
            exclude: ['password']
        },
        include: [{
            model: model.user_category,
            as: 'user_category',
            attributes: ['user_category_name']
        }, model.fleet_owner_auxilliary]
    }).then(data => {
        res.json({
            user: data
        })
    }, error => {
        console.log(error);
        res.json({
            stat: 'failure',
            code: 301
        })
    })
})

/*Login */
router.post('/login', (req, res) => {
    var mobile_no = body.mobile_no;
    var password = body.password;
    model.user.findOne({
        attributes: ['name', 'id', 'password'],
        where: {
            mobile_number: mobile_no
        }
    }).then(data => {
        if (data != null) {
            if (passwordHash.verify(password, data.password)) {
                res.json({
                    stat: 'success',
                    id: data.id,
                    mobile_no: mobile_no,
                    code: 100
                });
            } else {
                res.json({
                    stat: 'failure',
                    id: data.id,
                    message: 'wrong password',
                    code: 203
                });
            }
        } else {
            res.json({
                stat: 'nodata',
                code: 400
            });
        }
    }, err => {
        res.json({
            stat: 'error',
            code: 500
        });
        console.log(err.errMsg);
    })
})

/* Get users by category */
router.get('/bycategory/:cat_id', (req, res) => {
    model.user_category.findOne({where:{id:req.params.cat_id}}).then(xdata=>{
      if(xdata!=null){
        model.user.findAll({
            where:{
              userCategoryId:req.params.cat_id
            },
            attributes: {
                exclude: ['password']
            },
            include: [{
                model: model.user_category,
                as: 'user_category',
                attributes: ['user_category_name']
            }/*, model.fleet_owner_auxilliary*/]
        }).then(data => {
            res.json({
                user_count: data.length,
                users: data
            })
        }, error => {
            console.log(error);
            res.json({
                stat: 'failure',
                code: 301
            })
        })
      }else{
        res.json({
            stat: 'nodata',
            code: 303
        })
      }
    }, error=>{
      res.json({
          stat: 'failure',
          code: 400
      })
    })
})

module.exports = router;
