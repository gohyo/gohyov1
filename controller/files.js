var express = require('express');
var model = require('../model');
var multer  = require('multer');
var crypto=require('crypto');
var mime = require('mime');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + '.' + mime.extension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });
var router = express.Router();
router.post('/upload', upload.single('file'), (req,res)=>{
  model.file.create({
    file_path:req.file.path,
    file_name: req.file.originalname
  }).then(data=>{
    res.json({
      path: req.file.path,
      file_name: req.file.file_name,
      size: req.file.size,
      stat: 'success',
      code: 200
    })
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    });
    console.log(error);
  })
})
module.exports=router;
