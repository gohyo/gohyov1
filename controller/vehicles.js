var express = require('express');
var model = require('../model');
var router = express.Router();
/* Create new vehicle */
router.post('/', (req, res) => {
        var body = req.body;
        model.vehicle.create({
            vehicle_category_name: body.category_name,
            vehicle_make_name: body.vehicle_make_name,
            vehicle_number: body.vehicle_number,
            location: model.Sequelize.literal("point(0,0)"),
            schemeId: body.scheme_id,
            availability: 'V',
            fleetOwnerId: body.fleet_owner_id,
            vehicle_info: body.vehicle_info
        }).then(data => {
            res.json({
                stat: 'success',
                code: 200,
                id: data.id
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 300
            })
            console.log(error);
        })
    })
    /*Get all*/
router.get('/', (req, res) => {
        model.vehicle.findAll().then(data => {
            res.json({
                vehicles_count: data.length,
                vehicles: data
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 300
            })
        })
    })
    /*Get one*/
router.get('/:id', (req, res) => {
        model.vehicle.findAll({
            where: {
                id: req.params.id
            }
        }).then(data => {
            res.json({
                stat: 'success',
                vehicle: data
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 300
            })
        })
    })
    /*Update location*/
router.post('/locate', (req, res) => {
    var vehicle_id = req.body.vehicle_id;
    var lat = req.body.lat;
    var lng = req.body.lat;
    var literal = "point(" + lat + "," + lng + ")";
    console.log(literal)
    model.vehicle.update({
        location: model.Sequelize.literal(literal)
    }, {
        fields: ['location', 'id'],
        where: {
            id: vehicle_id
        }
    }).then(data => {
        data ? res.json({
            stat: 'success',
            code: 400
        }) : res.json({
            stat: 'success',
            code: 301
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
})
module.exports = router;
