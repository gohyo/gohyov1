var express = require('express');
var model = require('../model');
var router = express.Router();
/* Create a new module */
router.post('/', (req, res) => {
    var body = req.body;
    model.module.create({
        module_name: body.module_name
    }).then(data => {
        res.json({
            id: data.id,
            stat: 'success',
            code: 201
        });
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        });
        console.log(error);
    })
});
/* Get all modules */
router.get('/', (req, res)=>{
  model.module.findAll().then(data=>{
    res.json({
      module_count:data.length,
      modules:data
    })
  }, error=>{
    res.json({
      stat:'failure',
      code:301
    })
  })
})
/* Get Role by id */
router.get('/:id', (req,res)=>{

  model.module.findOne({id:req.params.id}).then(data=>{
    (data==null)?res.json({
      stat:'nodata',
      code:400
    }):res.json({
      module: data,
      code: 200
    })
  }, error=>{
    res.json({
      stat:'failure',
      code:301
    })
  })
})
/* Delete a Role */
router.delete('/:id', (req,res)=>{
  /*Check if module exists in admini users table*/

  //If module is unused the delete
  model.module.destroy({
    where:{
      id:req.params.id
    }
  }).then(data=>{
    (data==0)?res.json({
      stat:'nodata',
      code:400
    }):res.json({
      stat:'success',
      code:300
    })
  }, error=>{
    res.json({
      stat:'failure',
      code:300
    })
  })
})
/*Update a module*/
router.put('/:id', (req,res)=>{
  var body=req.body;
  var module_name=body.module_name;
  model.module.update({
    module_name:module_name
  },{
    where:{
      id:req.params.id
    }
  }).then(data=>{
    (data==0)?res.json({
      stat:'nodata',
      code:400
    }):res.json({
      stat:'success',
      code:300
    })
  }, error=>{
    res.json({
      stat:'failure',
      code:300
    })
  })
})
module.exports = router;
