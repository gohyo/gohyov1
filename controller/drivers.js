var express = require('express');
var model = require('../model');
var router = express.Router();

/* Register a new driver */

router.post('/',(req,res)=>{
    var body=req.body;
    model.driver.create({
      driver_name:body.driver_name,
      driver_address:body.driver_address,
      driver_mobile:body.driver_phone,
      driver_photo_path:body.profile_pic_path
    }).then(data => {
        res.json({
            id: data.id,
            stat: 'success',
            code: 200
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
        console.log(error);
    })
});

router.post('/docs',(req,res)=>{
  var body=req.body;
  var driver_docs=body.driver_docs;
  driver_docs.forEach(function(item){
    item['driverId']=body.driver_id;
  })
  model.driver.findOne({
    where:{
      id: body.driver_id
    }
  }).then(data1=>{
    if(data1!=null){
      model.driver_doc.bulkCreate(driver_docs).then(data=>{
        res.json({
          stat:'success',
          code: 200,
          count: driver_docs.length
        })
      }, error=>{
        res.json({
            stat: 'failure',
            message:'could not upload document data',
            code: 300
        })
      });
    }else{
      res.json({
        stat: 'nodata',
        code: 301
      })
    }
  }, error1=>{
    res.json({
        stat: 'failure',
        code: 300
    })
    console.log(error1);
  })
})

router.post('/pair', (req,res)=>{
  var body=req.body;
  model.driver_vehicle_pair.create({
    vehicleId:body.vehicle_id,
    driverId:body.driver_id,
    fleetOwnerId:body.fleet_owner_id
  }).then(data => {
      res.json({
          id: data.id,
          stat: 'success',
          code: 201
      });
  }, error => {
      res.json({
          stat: 'failure',
          code: 300
      });
      console.log(error);
  });
})
module.exports = router;
