var express = require('express');
var model = require('../model');
var router = express.Router();

/*Create scheme*/
router.post('/', (req,res)=>{
  var body=req.body;
  model.scheme.create({
    scheme_name: body.scheme_name,
    is_active: body.is_active,
    scheme_creator: body.scheme_creator
  }).then(data=>{
    res.json({
      stat:'success',
      code:200,
      id: data.id
    })
  }, error=>{
    res.json({
      stat:'failure',
      code:300
    })
  })
})
/*Get all schemes*/
router.get('/', (req,res)=>{
  model.scheme.findAll({
    include: [model.slab]
  }).then(data=>{
    res.json({
      scheme_count: data.length,
      schemes: data
    })
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    })
    console.log(error);
  })
})
/*Get specific scheme*/
router.get('/:id', (req,res)=>{
  model.scheme.findOne({
    where:{id: req.params.id},
    include: [model.slab]
  }).then(data=>{
    res.json({
      scheme: data
    })
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    })
  })
})
/*Create slabs*/
router.post('/slabs', (req,res)=>{
  var body=req.body;
  var scheme_id=body.scheme_id;
  var slabs=body.slabs;
  slabs.forEach(function(item){
    item['schemeId']=scheme_id;
    console.log(item);
  })
  model.slab.bulkCreate(slabs).then(data=>{
    res.json({
      stat:'success',
      code:200,
      count: data.length
    })
  }, error=>{
    res.json({
      stat:'failure',
      code:300
    })
    console.log(error);
  })
})
/*Get all slabs*/
router.get('/slabs', (req,res)=>{
  model.slabs.findAll().then(data=>{
    res.json({
      scheme_count: data.length,
      schemes: data
    })
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    })
  })
})
/*Get a specific slab*/
router.get('/slabs/:id', (req,res)=>{
  model.slabs.findAll({
    where:{
      id: req.params.id
    }
  }).then(data=>{
    res.json({
      slabs_count: data.length,
      slabs: data
    })
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    })
  })
})
/*edit a slab*/
router.put('/slabs/:id', (req,res)=>{
  var body=req.params.body;
  model.slabs.update(
    {
      slab_lower_limit: body.slab_lower_limit,
      slab_upper_limit: body.slab_upper_limit,
      rate: body.rate
    },
    {
    where:{
      id: req.params.id
    }
  }).then(data=>{
    res.json({
      slabs: data,
      code: 202
    })
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    })
  })
})

module.exports=router;
