var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var model = require('./model');
var roles=require('./controller/roles');
var modules=require('./controller/modules');
var users=require('./controller/users');
var schemes=require('./controller/schemes');
var files=require('./controller/files');
var vehicles=require('./controller/vehicles');
var drivers=require('./controller/drivers');
var app = express();
var authenticate=(req,res,next)=>{
  /*
  //var appId=
  */
  next();
  return;
}
app.use(authenticate);
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/roles', roles);
app.use('/modules', modules);
app.use('/users', users);
app.use('/schemes', schemes);
app.use('/files', files);
app.use('/vehicles', vehicles);
app.use('/drivers', drivers);

model.sequelize.sync({
    //logging: console.log,
    //force:true
}).then(function() {
  console.log("Setting up user category values");
  model.user_category.bulkCreate([
    {
      user_category_name: 'Retail'
    },
    {
      user_category_name: 'Generic'
    },
    {
      user_category_name: 'Individual'
    }
  ]).then(data=>{
    app.listen(3000);
    console.log('Server Started!');
    /*model.parent.create().then(data=>{
      model.child.create({
        parentId: data.id
      }).then(data=>{
        console.log({message: 'Created records...\n Searching', _value: 'abcd'});
        model.child.findAll({
          include:[{
            model: model.child,
            as: 'child'
          }]
        }).then(data=>{
          console.log(data[0].dataValues);
        }, error=>{
          console.log(error);
        })
      });
    })*/
  })

});
