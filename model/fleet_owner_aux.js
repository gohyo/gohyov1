module.exports=(sequelize, Sequelize) =>{
  var FleetOwner=sequelize.define("fleet_owner_auxilliary",{
    bank_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    bank_branch:{
      type: Sequelize.STRING,
      allowNull: false
    },
    bank_accntNo:{
      type: Sequelize.STRING(20),
      allowNull: false
    },
    bank_ifsc:{
      type: Sequelize.STRING(11),
      allowNull: false
    },
    tds_status:{
      type: Sequelize.STRING(10),
      allowNull: false
    },
    payment_period:{
      type: Sequelize.INTEGER,
      allowNull: false
    }
  },{
    freezeTableName: true
  });
  return FleetOwner;
}
