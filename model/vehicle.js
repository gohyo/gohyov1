module.exports=(sequelize, Sequelize) =>{
  var Vehicle=sequelize.define("vehicle",{
    vehicle_category_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    vehicle_make_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    vehicle_number:{
      type: Sequelize.STRING(15),
      allowNull: false
    },
    location:{
      type: 'point',
      defaultValue: sequelize.literal('point(0,0)')
    },
    availability:{
      type: Sequelize.STRING(1),
      default: 'V'
    }
  });
  return Vehicle;
}
