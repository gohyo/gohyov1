module.exports=(sequelize, Sequelize) =>{
  var Module=sequelize.define("module",{
    module_name:{
      type: Sequelize.STRING,
      allowNull: false
    }
  });
  return Module;
}
