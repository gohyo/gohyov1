var Sequelize=require('sequelize');
var config=require('config');
var conString=config.get('conString');
var fs        = require("fs");
var path      = require("path");
var sequelize = new Sequelize(conString);
var db        = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

/* Define Relations */
db.driver_doc.belongsTo(db.driver);
db.timestamp.belongsTo(db.order);
db.invoice_header.belongsTo(db.order);
db.invoice_header.belongsTo(db.invoice);
db.user.belongsTo(db.user_category);
db.individual_user_auxilliary.belongsTo(db.user);
db.user.hasOne(db.individual_user_auxilliary);
db.retail_corp_user_auxilliary.belongsTo(db.user);
db.user.hasOne(db.retail_corp_user_auxilliary);
db.scheme.hasMany(db.slab);
db.retail_corp_user_auxilliary.belongsTo(db.user);
db.fleet_owner_auxilliary.belongsTo(db.user);
db.corp_rate_chart.belongsTo(db.user);
db.vehicle.belongsTo(db.scheme);
db.vehicle.hasOne(db.driver_vehicle_pair);
db.driver.hasOne(db.driver_vehicle_pair);
db.user.hasOne(db.driver_vehicle_pair,{foreignKey:'fleetOwnerId'});
db.user.hasOne(db.vehicle, {foreignKey: 'fleetOwnerId'});
db.trip_daily.belongsTo(db.vehicle);
module.exports = db;
