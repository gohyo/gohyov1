module.exports=(sequelize, Sequelize) =>{
  var UserCategory=sequelize.define("user_category",{
    user_category_name:{
      type: Sequelize.STRING,
      allowNull: false
    }
  },{
    freezeTableName: true
  });
  return UserCategory;
}
