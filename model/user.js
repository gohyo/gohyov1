module.exports=(sequelize, Sequelize) =>{
  var User=sequelize.define("user",{
    name:{
      type: Sequelize.STRING(100),
      allowNull: false
    },
    email_id:{
      type: Sequelize.STRING(50),
      allowNull: false
    },
    password: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    mobile_number:{
      type: Sequelize.STRING(12),
      allowNull: false
    },
    phone_no:{
      type: Sequelize.STRING(12),
      allowNull: false
    },
    profile_pic_path:{
      type: Sequelize.STRING(255),
      allowNull: false
    },
    active:{
      type: Sequelize.BOOLEAN,
      defaultValue: false
    }
  });
  return User;
}
