module.exports=(sequelize, Sequelize) =>{
  var Driver=sequelize.define("driver",{
    driver_name:{
      type: Sequelize.STRING(200),
      allowNull: false
    },
    driver_address:{
      type: Sequelize.STRING(255),
      allowNull: true
    },
    driver_mobile:{
      type: Sequelize.STRING(12),
      allowNull:false
    },
    driver_photo_path:{
      type: Sequelize.STRING(255),
      defaultValue: 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
    }
  });
  return Driver;
}
