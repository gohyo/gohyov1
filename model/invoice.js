module.exports=(sequelize, Sequelize) =>{
  var Invoice=sequelize.define("invoice",{
    distance_amount:{
      type: Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    tax_amount:{
      type:Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    tax_description:{
      type: Sequelize.STRING(150),
      defaultValue: ''
    },
    tax_percentage:{
      type: Sequelize.DECIMAL(5,2),
      allowNull: false,
      defaultValue: 0.0
    },
    discount_amount:{
      type:Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    discount_description:{
      type: Sequelize.STRING(150),
      defaultValue: ''
    },
    discount_percentage:{
      type: Sequelize.DECIMAL(5,2),
      allowNull: false,
      defaultValue: 0.0
    },
    insurance_amount:{
      type:Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    penalty_amount:{
      type:Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    labour_amount:{
      type:Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    final_amount:{
      type:Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    payment_status:{
      type: Sequelize.STRING(20),
      allowNull: false,
      defaultValue: 'incomplete'
    }
  });
  return Invoice;
}
