module.exports=(sequelize, Sequelize) =>{
  var Role=sequelize.define("role",{
    role_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    module_ids:{
      type: Sequelize.STRING,
      allowNull: false
    }
  });
  return Role;
}
