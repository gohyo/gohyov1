module.exports=(sequelize, Sequelize) =>{
  var TripDaily=sequelize.define("trip_daily",{
    trip_distance:{
      type: Sequelize.DECIMAL(10,2),
      allowNull: false
    },
    empty_distance:{
      type: Sequelize.DECIMAL(10,2),
      allowNull: false
    }
  });
  return TripDaily;
}
